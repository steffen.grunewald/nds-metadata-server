//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package mds

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"git.ligo.org/nds/nds-metadata-server/common"
	"io"
	"net/http"
	"strings"
	"sync"
)

// Describes a remote endpoint that the metadata store pools data with
// This is to allow the metadata server to consume and distribute
// metadata with a network of servers serving a global multi site
// set of data.
//
// Note this is distinct from replication which is to simply mirror
// a server.
type RemoteDataStore struct {
	Label      string
	ConnString string
}

type RemoteProxyInterface interface {
	ProxyDataStream(frameType string, channels []string, span common.TimeSpan) (io.ReadCloser, error)
}

type RemoteInterface interface {
	ReplicationSummary() (ReplicationHeader, error)
	ReplicationStream(summary SummaryRequest) (io.ReadCloser, error)
	RemoteProxyInterface
}

type RemoteInterfaceFactory func(string) (RemoteInterface, error)

type httpRemoteInterface struct {
	address string
}

func (h *httpRemoteInterface) ReplicationSummary() (ReplicationHeader, error) {
	url := "http://" + h.address + "/replicate/summary/"
	resp, err := http.Get(url)
	if err != nil {
		return ReplicationHeader{}, err
	}
	defer resp.Body.Close()
	var header ReplicationHeader
	decoder := json.NewDecoder(resp.Body)
	if err = decoder.Decode(&header); err != nil {
		return ReplicationHeader{}, err
	}
	return header, nil
}

func (h *httpRemoteInterface) ReplicationStream(summaryReq SummaryRequest) (io.ReadCloser, error) {
	url := "http://" + h.address + "/replicate/"

	data, err := json.Marshal(&summaryReq)
	if err != nil {
		return nil, err
	}
	reqPayload := bytes.NewReader(data)
	resp, err := http.Post(url, "application/json", reqPayload)
	if err != nil {
		return nil, err
	}
	return resp.Body, nil
}

func (h *httpRemoteInterface) ProxyDataStream(frameType string, channels []string, span common.TimeSpan) (io.ReadCloser, error) {
	url := "http://" + h.address + fmt.Sprintf("/proxy/data/%s/%d/%d/", frameType, span.Start(), span.End())
	reader, writer := io.Pipe()
	go func(writer io.WriteCloser, channels []string) {
		defer writer.Close()
		encoder := json.NewEncoder(writer)
		for _, entry := range channels {
			encoder.Encode(&entry)
		}
	}(writer, channels)

	resp, err := http.Post(url, "application/json", reader)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("Error getting proxy")
	}
	return resp.Body, nil
}

var remoteInterfaces map[string]RemoteInterfaceFactory
var remoteInterfacesMutex sync.Mutex

func init() {
	remoteInterfaces = make(map[string]RemoteInterfaceFactory)
	remoteInterfaces["nds3"] = func(conn string) (RemoteInterface, error) {
		return &httpRemoteInterface{address: conn}, nil
	}
}

func RegisterRemoteInterfaceFactory(protocol string, factory RemoteInterfaceFactory) {
	remoteInterfacesMutex.Lock()
	defer remoteInterfacesMutex.Unlock()

	remoteInterfaces[protocol] = factory
}

func removeRemoteInterfaceFactory(protocol string) {
	remoteInterfacesMutex.Lock()
	defer remoteInterfacesMutex.Unlock()

	if _, ok := remoteInterfaces[protocol]; ok {
		delete(remoteInterfaces, protocol)
	}
}

func GetRemoteInterface(connStr string) (RemoteInterface, error) {
	remoteInterfacesMutex.Lock()
	defer remoteInterfacesMutex.Unlock()

	parts := strings.SplitN(connStr, "://", 2)
	if len(parts) != 2 {
		return nil, errors.New("Invalid connection string, <protocol>://<string> required")
	}
	if factory, ok := remoteInterfaces[parts[0]]; !ok {
		return nil, errors.New("Remote interface handler not found")
	} else {
		return factory(parts[1])
	}
}
