//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package mds

import (
	"bytes"
	"errors"
	"fmt"
	"git.ligo.org/nds/nds-metadata-server/common"
	"io"
	"io/ioutil"
	"sync/atomic"
)

type testRemoteInterface struct {
	mds   *MetaDataStore
	proto string
}

func newTestRemoteInterface(mds *MetaDataStore) *testRemoteInterface {
	return &testRemoteInterface{mds: mds}
}

func (t *testRemoteInterface) ReplicationSummary() (ReplicationHeader, error) {
	if t.mds == nil {
		return ReplicationHeader{}, errors.New("Not available")
	}
	return t.mds.GetReplicationSummary(), nil
}

func (t *testRemoteInterface) ReplicationStream(summaryRequest SummaryRequest) (io.ReadCloser, error) {
	if t.mds == nil {
		return nil, errors.New("Not available")
	}
	buffer := &bytes.Buffer{}
	if err := t.mds.ReplicationStream(buffer, summaryRequest); err != nil {
		return nil, err
	}
	return ioutil.NopCloser(buffer), nil
}

func (h *testRemoteInterface) ProxyDataStream(frameType string, channels []string, span common.TimeSpan) (io.ReadCloser, error) {
	return nil, errors.New("Not implemented")
}

func (t *testRemoteInterface) swapDataStore(mds *MetaDataStore) {
	t.mds = mds
}

var testRemoteInterfaceCounter int32

func (t *testRemoteInterface) connStr() string {
	t.register()
	return t.proto + "://1"
}
func (t *testRemoteInterface) register() {
	if t.proto != "" {
		return
	}
	val := atomic.AddInt32(&testRemoteInterfaceCounter, 1)
	t.proto = fmt.Sprintf("test_remote__%d", val)
	RegisterRemoteInterfaceFactory(t.proto, func(connStr string) (RemoteInterface, error) {
		return t, nil
	})
}

func (t *testRemoteInterface) unregister() {
	if t.proto != "" {
		removeRemoteInterfaceFactory(t.proto)
		t.proto = ""
	}
}
