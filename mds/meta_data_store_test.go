//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package mds

import (
	"fmt"
	"git.ligo.org/nds/nds-metadata-server/common"
	"git.ligo.org/nds/nds-metadata-server/data_test"
	"os"
	"reflect"
	"testing"
)

func TestNewFrameType(t *testing.T) {
	ft1 := NewFrameType("", "abc")
	if ft1.FrameName != "abc" || ft1.FrameLabel != "" {
		t.Fail()
		t.Logf("Bad structure on frame type")
	}
	if ft1.PrintName != "abc" {
		t.Fail()
		t.Logf("Could not properly create a local frame type expected \"abc\" got \"%s\"", ft1.PrintName)
	}

	ft2 := NewFrameType("r1", "def")
	if ft2.FrameName != "def" || ft2.FrameLabel != "r1" {
		t.Fail()
		t.Logf("Bad structure on frame type")
	}
	if ft2.PrintName != "(r1)def" {
		t.Fail()
		t.Logf("Could not properly create a local frame type expected \"(r1)def\" got \"%s\"", ft2.PrintName)
	}
}

func TestMetaDataStore_GetReplicationSummary(t *testing.T) {
	mds, _ := LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream1Reader())
	summary := mds.GetReplicationSummary()

	if summary.NumChannels != 12 {
		t.Fail()
		t.Logf("Wrong channel count expected 12 got %d", summary.NumChannels)
	}
	if len(summary.FrameTypes) != 1 || summary.FrameTypes[0] != "H-H1_HOFT_C00" {
		t.Fail()
		t.Logf("Wrong frame type list")
	}
	if len(summary.ChannelConfig) == 0 {
		t.Fail()
		t.Logf("Empty channel config hash found")
	}
}

func Test_createMetaDataStore(t *testing.T) {
	fr := CreateFrameTypeRegistrar()
	fr.AddChannelListFromReader(data_test.SampleChanList2FromReader())
	frame_server, _ := common.GetFrameServer("null_frames://null")
	mds := createMetaDataStore(fr, frame_server)
	if len(mds.db) != 44 {
		t.Fail()
		t.Log("Wrong channel count from frame registrar")
	}
	if len(mds.db) != mds.localchannelCount {
		t.Fail()
		t.Log("The channel count should equal the local channel count")
	}
	if mds.localChannelChecksum == nil || len(mds.localChannelChecksum) == 0 {
		t.Fail()
		t.Log("No local channel checksum found")
	}
}

func TestMetaDataStore_AddRemote(t *testing.T) {
	mds, _ := LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream1Reader())
	mds2, _ := LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream2Reader())
	remoteIface := newTestRemoteInterface(mds2)
	defer remoteIface.unregister()

	err := mds.AddRemote(RemoteDataStore{Label: "L1", ConnString: remoteIface.connStr()})
	if err != nil {
		t.Fail()
		t.Logf("Unexpected error when adding a remote data store, %v", err)
	}
	if len(mds.remoteServers) != 1 {
		t.Fail()
		t.Log("Did not add an entry to the remote list")
	}
}

func TestMetaDataStore_AddRemote_BadInput(t *testing.T) {
	mds, _ := LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream1Reader())

	err := mds.AddRemote(RemoteDataStore{Label: "L1", ConnString: ""})
	if err == nil {
		t.Fail()
		t.Log("Err was nil when one was expected")
	}
	if len(mds.remoteServers) != 0 {
		t.Fail()
		t.Log("Did added an entry to the remote list when it should not have")
	}
}

func TestMetaDataStore_UpdateRemote(t *testing.T) {
	mds, _ := LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream1Reader())
	mds2, _ := LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream2Reader())
	mds3, _ := LoadDatabaseFromReplicationStream(data_test.SampleDbReplicationStream3Reader())
	remoteIface := newTestRemoteInterface(mds2)
	defer remoteIface.unregister()

	_ = mds.AddRemote(RemoteDataStore{Label: "L1", ConnString: remoteIface.connStr()})
	err := mds.UpdateRemotes()
	if err != nil {
		t.Fail()
		t.Logf("Update on remotes failed with error, %v", err)
	}
	if len(mds.db) != 26 {
		t.Fail()
		t.Logf("Wrong channel list, expected 26 channels, got %d", len(mds.db))
		mds.DebugDump(os.Stderr, len(mds.db))
		t.Fatalf("Aborting test")
	}

	fmt.Println("----------------")
	fmt.Println("cur data")
	mds.DebugDump(os.Stdout, len(mds.db))
	fmt.Println("----------------")
	fmt.Println("new remote data")
	mds3.DebugDump(os.Stdout, len(mds3.db))
	fmt.Println("----------------")

	remoteIface.swapDataStore(mds3)
	err = mds.UpdateRemotes()
	if err != nil {
		t.Fail()
		t.Logf("Update on remotes failed with error, %v", err)
	}
	if len(mds.db) != 24 {
		t.Fail()
		t.Logf("Wrong channel list, expected 24 channels, got %d", len(mds.db))
		mds.DebugDump(os.Stderr, len(mds.db))
	}

}

func Test_rebuildLocalChannelListWithRemote(t *testing.T) {
	type args struct {
		localChannels       []ChannelAndAvailability
		remoteChannels      []ChannelAndAvailability
		oldRemoteFrameTypes []int
	}
	tests := []struct {
		name string
		args args
		want []ChannelAndAvailability
	}{
		{
			name: "basic test",
			args: args{
				localChannels: []ChannelAndAvailability{
					{
						Channel: BasicChannel{
							Name: "CHAN1", Type: common.CreateDataType(common.DataTypeInt32),
							Class: common.CreateClassType(common.ClassTypeRaw), Rate: 1024,
						},
						Avail: BasicAvailability{
							Existence: TimeSpan{0, common.GpsInf},
							Availability: []AvailabilitySpan{
								{
									FrameType: 0,
									Time:      CreateTimeSpan(0, common.GpsInf),
								},
							},
						},
						Local: true,
					},
					{
						Channel: BasicChannel{
							Name: "CHAN2", Type: common.CreateDataType(common.DataTypeInt32),
							Class: common.CreateClassType(common.ClassTypeRaw), Rate: 1024,
						},
						Avail: BasicAvailability{
							Existence: TimeSpan{0, common.GpsInf},
							Availability: []AvailabilitySpan{
								{
									FrameType: 3,
									Time:      CreateTimeSpan(0, common.GpsInf),
								},
							},
						},
						Local: false,
					},
					{
						Channel: BasicChannel{
							Name: "CHAN_TO_REMOVE", Type: common.CreateDataType(common.DataTypeInt32),
							Class: common.CreateClassType(common.ClassTypeRaw), Rate: 1024,
						},
						Avail: BasicAvailability{
							Existence: TimeSpan{0, common.GpsInf},
							Availability: []AvailabilitySpan{
								{
									FrameType: 1,
									Time:      CreateTimeSpan(0, common.GpsInf),
								},
							},
						},
						Local: false,
					},
				},
				remoteChannels: []ChannelAndAvailability{
					{
						Channel: BasicChannel{
							Name: "CHAN1", Type: common.CreateDataType(common.DataTypeInt32),
							Class: common.CreateClassType(common.ClassTypeRaw), Rate: 1024,
						},
						Avail: BasicAvailability{
							Existence: TimeSpan{0, common.GpsInf},
							Availability: []AvailabilitySpan{
								{
									FrameType: 2,
									Time:      CreateTimeSpan(0, common.GpsInf),
								},
							},
						},
						Local: false,
					},
					{
						Channel: BasicChannel{
							Name: "CHAN3", Type: common.CreateDataType(common.DataTypeInt32),
							Class: common.CreateClassType(common.ClassTypeRaw), Rate: 1024,
						},
						Avail: BasicAvailability{
							Existence: TimeSpan{0, common.GpsInf},
							Availability: []AvailabilitySpan{
								{
									FrameType: 2,
									Time:      CreateTimeSpan(0, common.GpsInf),
								},
							},
						},
						Local: false,
					},
				},
				oldRemoteFrameTypes: []int{1},
			},
			want: []ChannelAndAvailability{
				{
					Channel: BasicChannel{
						Name: "CHAN1", Type: common.CreateDataType(common.DataTypeInt32),
						Class: common.CreateClassType(common.ClassTypeRaw), Rate: 1024,
					},
					Avail: BasicAvailability{
						Existence: TimeSpan{0, common.GpsInf},
						Availability: []AvailabilitySpan{
							{
								FrameType: 0,
								Time:      CreateTimeSpan(0, common.GpsInf),
							},
							{
								FrameType: 2,
								Time:      CreateTimeSpan(0, common.GpsInf),
							},
						},
					},
					Local: true,
				},
				{
					Channel: BasicChannel{
						Name: "CHAN2", Type: common.CreateDataType(common.DataTypeInt32),
						Class: common.CreateClassType(common.ClassTypeRaw), Rate: 1024,
					},
					Avail: BasicAvailability{
						Existence: TimeSpan{0, common.GpsInf},
						Availability: []AvailabilitySpan{
							{
								FrameType: 3,
								Time:      CreateTimeSpan(0, common.GpsInf),
							},
						},
					},
					Local: false,
				},
				{
					Channel: BasicChannel{
						Name: "CHAN3", Type: common.CreateDataType(common.DataTypeInt32),
						Class: common.CreateClassType(common.ClassTypeRaw), Rate: 1024,
					},
					Avail: BasicAvailability{
						Existence: TimeSpan{0, common.GpsInf},
						Availability: []AvailabilitySpan{
							{
								FrameType: 2,
								Time:      CreateTimeSpan(0, common.GpsInf),
							},
						},
					},
					Local: false,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := rebuildLocalChannelListWithRemote(tt.args.localChannels, tt.args.remoteChannels, tt.args.oldRemoteFrameTypes); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("rebuildLocalChannelListWithRemote() = %v, want %v", got, tt.want)
			}
		})
	}
}
