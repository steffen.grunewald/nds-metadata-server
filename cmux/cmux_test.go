//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package cmux

import (
	"errors"
	"io"
	"net"
	"testing"
)

type dummyListener struct {
	address net.Addr
	closed  bool
}

func (d *dummyListener) Accept() (net.Conn, error) {
	return nil, errors.New("Not implemented")
}

func (d *dummyListener) Close() error {
	if d.closed {
		return errors.New("Already closed")
	}
	d.closed = true
	return nil
}

func (d *dummyListener) Addr() net.Addr {
	return d.address
}

func TestNewCMux(t *testing.T) {
	l := &dummyListener{address: &net.TCPAddr{IP: net.IPv4(127, 0, 0, 1), Port: 3000}}
	cm := NewCMux(l)

	if cm.listener != l {
		t.Fail()
		t.Logf("The underlying listener is not right")
	}

	if cm.Addr() != l.Addr() {
		t.Fail()
		t.Logf("CMux's Addr() doesn't return the same as the underlying listener")
	}

	if err := cm.Close(); err != nil {
		t.Fail()
		t.Logf("Close() returned %v", err)
	}
	if l.closed != true {
		t.Fail()
		t.Logf("The underlying listener was not closed")
	}
}

func dummyMatcher([]byte) bool {
	return false
}

func TestCMux_RegisterProtocol(t *testing.T) {
	l := &dummyListener{address: &net.TCPAddr{IP: net.IPv4(127, 0, 0, 1), Port: 3000}}
	cm := NewCMux(l)
	myListener, err := cm.RegisterProtocol(dummyMatcher)
	if err != nil {
		t.Fatalf("Unable to register a protocol, %v", err)
	}

	if myListener.Addr() != l.Addr() {
		t.Fail()
		t.Log("The protocol listener does have the same address as the underlying listener")
	}
	err = myListener.Close()
	if err != nil {
		t.Fail()
		t.Fatalf("Closing the protocol listener resulted in an error, %v", err)
	}
}

func helloMatcher(data []byte) bool {
	if len(data) >= 5 && string(data[0:5]) == "hello" {
		return true
	}
	return false
}

func TestProtocolListener_Accept(t *testing.T) {
	l, err := net.Listen("tcp", "")
	if err != nil {
		t.Fatal("Unable to open underlying listener for testing", err)
	}
	cm := NewCMux(l)
	defer cm.Close()
	hello, err := cm.RegisterProtocol(helloMatcher)
	if err != nil {
		t.Fatal("Unable to register test handler ", err)
	}
	var clientConn1 net.Conn
	var clientConn2 net.Conn

	defer func() {
		if clientConn1 != nil {
			_ = clientConn1.Close()
		}
		if clientConn2 != nil {
			_ = clientConn2.Close()
		}
	}()
	go func() {
		var err error
		clientConn1, err = net.Dial("tcp", cm.Addr().String())
		if err != nil {
			t.Fatal("Error on Dial", err)
		}
		_, _ = clientConn1.Write([]byte("goodbye"))

		clientConn2, err = net.Dial("tcp", cm.Addr().String())
		if err != nil {
			t.Fatal("Error on Dial", err)
		}
		_, _ = clientConn2.Write([]byte("hello world!"))
	}()
	cm.Start()
	serverConn, err := hello.Accept()
	if err != nil {
		t.Fatal("Unable to accept ", err)
	}
	defer serverConn.Close()
	t.Logf("first accept complete")
	buffer := make([]byte, len("hello world!"))
	_, err = io.ReadAtLeast(serverConn, buffer, len(buffer))
	t.Logf("Err on read, %v", err)
	if string(buffer) != "hello world!" {
		t.Fatalf("Did not get the expected output from the connection %s", string(buffer))
	}
}
