//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

// The cmux package creates a multiplexer over connections.
// The point is to register accept/protocol handlers that can
// consume a few bytes of their traffic to determine if a connection
// matches their protocol.  If so, handle it, otherwise move onto the next
package cmux

import (
	"errors"
	"log"
	"net"
	"sync"
	"sync/atomic"
	"time"
)

type ProtocolMatcher func([]byte) bool

func MatchAll([]byte) bool {
	return true
}

type multiplexedConn struct {
	net.Conn
	start     byte
	remaining byte
	buffer    [10]byte
}

func (mc *multiplexedConn) Read(dest []byte) (int, error) {
	if mc.remaining == 0 {
		return mc.Conn.Read(dest)
	}
	myLen := int(mc.remaining)
	destLen := len(dest)
	var transferLen int
	if myLen < destLen {
		transferLen = myLen
	} else {
		transferLen = destLen
	}
	start := int(mc.start)
	log.Printf("Call to Read([%d]byte) rem=%d, start=%d", len(dest), int(mc.remaining), int(mc.start))
	copy(dest[0:transferLen], mc.buffer[start:start+transferLen])
	mc.remaining -= byte(transferLen)
	mc.start += byte(transferLen)
	//	if mc.remaining != 0 || transferLen == len(dest) {
	return transferLen, nil
	//	}
	//	additional, err := mc.Conn.Read(dest[transferLen:])
	//	return transferLen + additional, err
}

type protocol struct {
	Matcher ProtocolMatcher
	Queue   chan net.Conn
	Closed  bool
}

type classifiedConn struct {
	Conn net.Conn
	Id   int
}

type CMux struct {
	listener          net.Listener
	matchers          []protocol
	closed            int32
	classifySignal    chan classifiedConn
	closeSignal       chan int
	lock              sync.Mutex
	acceptLoopStarted bool
}

type protocolListener struct {
	queue       chan net.Conn
	closed      int32
	cmux        *CMux
	closeSignal chan int
	id          int
}

func (p *protocolListener) Accept() (net.Conn, error) {
	if atomic.LoadInt32(&p.closed) != 0 {
		return nil, errors.New("Listener closed")
	}
	if conn, ok := <-p.queue; ok {
		return conn, nil
	} else {
		atomic.StoreInt32(&p.closed, 1)
		return nil, errors.New("Listener closed")
	}
}

func (p *protocolListener) Close() error {
	if atomic.LoadInt32(&p.closed) != 0 {
		return nil
	}
	p.closeSignal <- p.id
	atomic.StoreInt32(&p.closed, 1)
	return nil
}

func (p *protocolListener) Addr() net.Addr {
	return p.cmux.Addr()
}

func NewCMux(listener net.Listener) CMux {
	return CMux{listener: listener,
		matchers:       make([]protocol, 0, 2),
		classifySignal: make(chan classifiedConn, 1000),
		closeSignal:    make(chan int, 2),
	}
}

func (c *CMux) shutdown(id int) {
	if id < 0 || id >= len(c.matchers) || c.matchers[id].Closed {
		return
	}
	log.Printf("Shutdown %d", id)
	c.matchers[id].Closed = true
	close(c.matchers[id].Queue)
	for conn := range c.matchers[id].Queue {
		_ = conn.Close()
	}
}

func (c *CMux) dispatch(classification classifiedConn) {
	id := classification.Id
	if id < 0 || id >= len(c.matchers) || c.matchers[id].Closed {
		_ = classification.Conn.Close()
		log.Printf("Invalid id or matcher closed")
		return
	}
	if len(c.matchers[id].Queue) == cap(c.matchers[id].Queue) {
		_ = classification.Conn.Close()
		log.Printf("Queue full %d/%d %v", len(c.matchers[id].Queue), cap(c.matchers[id].Queue), c.matchers[id].Queue)
		log.Print(c.matchers)
		return
	}
	log.Printf("Sending classified connection to its queue %d", id)
	c.matchers[id].Queue <- classification.Conn
}

func (c *CMux) classifyConn(conn net.Conn, counter *int32) {
	defer atomic.AddInt32(counter, -1)
	wrapped := &multiplexedConn{
		Conn:      conn,
		remaining: 0,
		buffer:    [10]byte{},
	}
	defer func(c *net.Conn) {
		if *c != nil {
			conn.Close()
		}
	}(&conn)
	log.Printf("About to read from conn %v", conn)
	n, err := conn.Read(wrapped.buffer[:])
	log.Printf("Read %d, %v from conn %v", n, err, conn)
	if err != nil && n == 0 {
		log.Printf("aborting classification")
		return
	}
	for id, _ := range c.matchers {
		if c.matchers[id].Matcher(wrapped.buffer[0:n]) {
			log.Printf("Found match for '%s' at %d", string(wrapped.buffer[0:n]), id)
			wrapped.remaining = byte(n)
			c.classifySignal <- classifiedConn{
				Conn: wrapped,
				Id:   id,
			}
			conn = nil
			return
		}
	}
	log.Printf("Could not classify")
}

func (c *CMux) acceptLoop() {
	counter := int32(0)
	for !c.isClosed() {
		conn, err := c.listener.Accept()
		if err != nil {
			continue
		}
		log.Printf("Accepted a conn, classifying %v", conn)
		atomic.AddInt32(&counter, 1)
		go c.classifyConn(conn, &counter)
	}
	log.Printf("acceptLoop ending, waiting for %d classifiers", atomic.LoadInt32(&counter))
	for atomic.LoadInt32(&counter) > 0 {
		time.Sleep(time.Millisecond * 50)
	}
	log.Printf("acceptLoop closing classifySignal")
	close(c.classifySignal)
	for conn := range c.classifySignal {
		_ = conn.Conn.Close()
	}
	log.Printf("acceptLoop exiting %v", c)
}

func (c *CMux) eventLoop() {
	for !c.isClosed() {
		select {
		case closeId := <-c.closeSignal:
			c.shutdown(closeId)
		case classification := <-c.classifySignal:
			log.Printf("Accepted a classified connection")
			c.dispatch(classification)
		}
	}
	log.Printf("eventLoop exiting %v", c)
}

func (c *CMux) Start() error {
	log.Printf("CMux.Start called")
	c.lock.Lock()
	defer c.lock.Unlock()

	if c.isClosed() {
		return errors.New("The multiplexer has already been closed")
	}
	if !c.acceptLoopStarted {
		c.acceptLoopStarted = true
		go c.eventLoop()
		go c.acceptLoop()
	}
	return nil
}

func (c *CMux) Close() error {
	log.Printf("CMux.Close called")
	c.lock.Lock()
	defer c.lock.Unlock()

	if c.isClosed() {
		return nil
	}
	atomic.StoreInt32(&c.closed, 1)
	if !c.acceptLoopStarted {
		for i, _ := range c.matchers {
			close(c.matchers[i].Queue)
		}
	} else {
		c.closeSignal <- -1
	}

	return c.listener.Close()
}

func (c *CMux) Addr() net.Addr {
	return c.listener.Addr()
}

func (c *CMux) isClosed() bool {
	return atomic.LoadInt32(&c.closed) != 0
}

func (c *CMux) RegisterProtocol(match ProtocolMatcher) (net.Listener, error) {
	c.lock.Lock()
	defer c.lock.Unlock()
	if c.acceptLoopStarted {
		return nil, errors.New("The accept loop has started, no more protocols can be registered")
	}

	if c.isClosed() {
		return nil, errors.New("The listener has been closed already")
	}
	listener := &protocolListener{
		queue:       make(chan net.Conn, 100),
		closed:      0,
		cmux:        c,
		closeSignal: c.closeSignal,
		id:          len(c.matchers),
	}
	log.Printf("New queue has len %d cap %d %v", len(listener.queue), cap(listener.queue), listener.queue)
	c.matchers = append(c.matchers, protocol{
		Matcher: match,
		Queue:   listener.queue,
	})
	log.Printf("%v", c.matchers[len(c.matchers)-1].Queue)
	log.Print(c.matchers)
	return listener, nil
}
