//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//
#ifndef NDS_FRAME_INDEXER_H
#define NDS_FRAME_INDEXER_H

#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

#define INDEX_CHANNEL_NAME_LEN 128

typedef struct indexed_channel {
    char name[INDEX_CHANNEL_NAME_LEN];
    double rate;
    int class_type;
    int data_type;
} indexed_channel;

extern const int FR_DATA_TYPE_Unknown;
extern const int FR_DATA_TYPE_Int16;
extern const int FR_DATA_TYPE_Int32;
extern const int FR_DATA_TYPE_Int64;
extern const int FR_DATA_TYPE_Float32;
extern const int FR_DATA_TYPE_Float64;
extern const int FR_DATA_TYPE_Complex32;
extern const int FR_DATA_TYPE_Uint32;

extern const int FR_CLASS_TYPE_Unknown;
extern const int FR_CLASS_TYPE_Online;
extern const int FR_CLASS_TYPE_Raw;
extern const int FR_CLASS_TYPE_Reduced;
extern const int FR_CLASS_TYPE_STrend;
extern const int FR_CLASS_TYPE_MTrend;
extern const int FR_CLASS_TYPE_TestPt;
extern const int FR_CLASS_TYPE_Static;

extern indexed_channel* index_frame(const char* path, size_t path_len, int *channel_count);
extern void free_index_channel_list(indexed_channel* channels);
#ifdef __cplusplus
}
#endif


#endif /* NDS_FRAME_INDEXER_H */