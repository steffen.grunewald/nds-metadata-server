# NDS Metadata Server #

Some thoughts and tests on a meta-data only server for LIGO NDS use.

A NDS meta-data server is the core of the NDS system.  It tracks the channels in the LIGO database as well as the
availability of the data through time.  This repository contains an implemenation of a
meta-data server for NDS, the components used to build it, and additional tools that take advantage of
the server.

This is a re-implementation of a C++ version done in Go.

## Source Repository

The main repository for this is currently located at https://git.ligo.org/nds/nds-metadata-server.

## Basic components:
 * common - Common data structures and defines
 * disk_cache - A custom DiskCacheAPI client api, both a blocking and non-blocking async-io based versions.
   * The disk cache is used to determine frame availability.
 * mds - The meta data servers core data structures.
 * nds2_parser - A parser for the nds2 language and for auxiliary formats needed by the system.

## Software Requirements
The software requires:
 * Go (tested with 1.14)
 * pkg-config
 * ldas-tools-framecpp-dev (2.7+)

## Building the software ##
 
GOPATH=`pwd` go build nds_metadata_server

## Code style

go fmt is used to format the go source code.
