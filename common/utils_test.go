//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package common

import (
	"testing"
)

var input []int

func init() {
	input = []int{1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6}
}

func greaterThan(val int) func(int) bool {
	return func(index int) bool {
		result := input[index] > val
		//log.Printf("gt(%d) %d > %d == %v", index, input[index], val, result)
		return result
	}
}

func greaterThanEqual(val int) func(int) bool {
	return func(index int) bool {
		result := input[index] >= val
		//log.Printf("gt(%d) %d >= %d == %v", index, input[index], val, result)
		return result
	}
}

func TestFindRange(t *testing.T) {
	var testCases = []struct {
		lower      RangePredicate
		upper      RangePredicate
		start, end int
	}{
		{greaterThanEqual(3), greaterThan(42), 6, 18},
		{greaterThanEqual(3), greaterThan(3), 6, 9},
		{greaterThanEqual(42), greaterThan(42), 18, 18},
		{greaterThanEqual(1), greaterThan(1), 0, 3},
		{greaterThanEqual(-1), greaterThan(0), 0, 0},
		{greaterThanEqual(6), greaterThan(6), 15, 18},
	}
	for _, testCase := range testCases {
		start, end := FindRange(len(input), testCase.lower, testCase.upper)
		if start != testCase.start || end != testCase.end {
			t.Fail()
			t.Logf("Expected range of [%d, %d) got [%d, %d)", testCase.start, testCase.end, start, end)
		} else {
			//	t.Log(input[start:end])
		}
	}

}
