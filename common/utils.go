//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package common

import (
	"bufio"
	"io"
	"sort"
	"strings"
)

func MinInt64(a int64, b int64) int64 {
	if a < b {
		return a
	}
	return b
}

func MaxInt64(a int64, b int64) int64 {
	if a > b {
		return a
	}
	return b
}

func MinInt(a int, b int) int {
	if a < b {
		return a
	}
	return b
}

func MaxInt(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func MinInt32(a int32, b int32) int32 {
	if a < b {
		return a
	}
	return b
}

func MaxInt32(a int32, b int32) int32 {
	if a > b {
		return a
	}
	return b
}

type ProcessStreamHandler func(string)

func ProcessStreamWithComments(reader io.Reader, callback ProcessStreamHandler) {
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		line := strings.TrimSpace(strings.SplitN(scanner.Text(), "#", 2)[0])
		if line == "" {
			continue
		}
		callback(line)
	}
}

func FilterOutEmpty(sequence []string) []string {
	out := make([]string, 0, len(sequence))
	for _, val := range sequence {
		if len(val) != 0 {
			out = append(out, val)
		}
	}
	return out
}

type RangePredicate func(int) bool

// Find the indexes for a sub-range that is bounded by lowerBoundPred an upperBoundPred
//
// n is the total number of elements in the range
// The lower bound of the returned indexes is the first entry where lowerBoundPred(i) == true
// The upper bound of the returned indexes is the first entry where upperBoundPred(i) == true
//
// This is implemented as calls to sort.Search with controlled ranges of entries.
//
// It may be helpful to think of identifying a range by passing in a gte(x) as a lower bound,
// to identify the first entry >= x, and gt(x) to identify the end of the sequence.
func FindRange(n int, lowerBoundPred RangePredicate, upperBoundPred RangePredicate) (int, int) {
	start := sort.Search(n, lowerBoundPred)
	//log.Printf("FindRange lower = %d out of %d", start, n)
	if start == n {
		return n, n
	}
	end := sort.Search(n-start, func(index int) bool {
		return upperBoundPred(index + start)
	})
	return start, end + start
}
