//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package common

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

type FrameServer interface {
	FrameIntervals(frameType string, timeSpan TimeSpan) ([]TimeSpan, error)
	FrameFilePaths(frameType string, timeSpan TimeSpan) ([]string, error)
	ConnectionString() string
}

type FrameInfo struct {
	Name, Path string
	FrameType  string
	TimeSpan   TimeSpan
	FTime      time.Time
}

type LocalFrameServer struct {
	Root   string
	frames map[string][]FrameInfo
}

func pathToFrameInfo(curPath string) (FrameInfo, error) {
	if filepath.Ext(curPath) != ".gwf" {
		return FrameInfo{}, errors.New("invalid frame file name")
	}
	baseName := filepath.Base(curPath[:len(curPath)-len(".gwf")])
	parts := strings.Split(baseName, "-")
	if len(parts) < 3 {
		return FrameInfo{}, errors.New("invalid frame file name")
	}
	duration, _ := strconv.ParseInt(parts[len(parts)-1], 10, 64)
	start, _ := strconv.ParseInt(parts[len(parts)-2], 10, 64)
	frameType := strings.Join(parts[:len(parts)-2], "-")
	return FrameInfo{Name: baseName, Path: curPath, TimeSpan: CreateTimeSpan(start, start+duration), FrameType: frameType}, nil
}

func FindFramesUnderPath(path string) map[string][]FrameInfo {
	framesByType := make(map[string][]FrameInfo)
	frameNames := make(map[string]bool)
	if _, err := os.Stat(path); err != nil {
		return make(map[string][]FrameInfo)
	}
	filepath.Walk(path, func(curPath string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		fInfo, err := pathToFrameInfo(curPath)
		if err != nil {
			return nil
		}
		fInfo.FTime = info.ModTime()
		if _, ok := frameNames[fInfo.Name]; ok {
			return nil
		}
		frameNames[fInfo.Name] = true
		fList, ok := framesByType[fInfo.FrameType]
		if !ok {
			fList = make([]FrameInfo, 0, 10)
		}
		fList = append(fList, fInfo)
		framesByType[fInfo.FrameType] = fList
		return nil
	})
	return framesByType
}

func NewLocalFrameServer(path string) *LocalFrameServer {
	fs := &LocalFrameServer{Root: path,
		frames: FindFramesUnderPath(path)}

	if val := os.Getenv("DUMP_METADATA_LOCAL_FRAMES"); val != "" {
		fmt.Println("Frame root", fs.Root)
		for frameType, frameList := range fs.frames {
			fmt.Println(frameType)
			for _, entry := range frameList {
				fmt.Printf("\t[%d, %d) %s\n", entry.TimeSpan.Start(), entry.TimeSpan.End(), entry.Path)
			}
		}
	}
	return fs
}

func (lf *LocalFrameServer) frameFileOperation(frameType string, span TimeSpan, op func(TimeSpan, *FrameInfo)) {
	frameList, ok := lf.frames[frameType]
	if !ok {
		return
	}
	for _, frame := range frameList {
		cur := frame.TimeSpan
		if TimeSpansOverlap(span, cur) {
			op(ConstrainTimeSpans(span, cur), &frame)
		}
	}
}

func (lf *LocalFrameServer) FrameIntervals(frameType string, timeSpan TimeSpan) ([]TimeSpan, error) {
	results := make([]TimeSpan, 0, 10)
	lf.frameFileOperation(frameType, timeSpan, func(curSpan TimeSpan, info *FrameInfo) {
		lastIndex := len(results) - 1
		if lastIndex >= 0 && results[lastIndex].End() == curSpan.Start() {
			results[lastIndex].Extend(curSpan.End())
			return
		}
		results = append(results, curSpan)
	})
	return results, nil
}

func (lf *LocalFrameServer) FrameFilePaths(frameType string, timeSpan TimeSpan) ([]string, error) {
	results := make([]string, 0, 10)

	lf.frameFileOperation(frameType, timeSpan, func(curSpan TimeSpan, info *FrameInfo) {
		results = append(results, info.Path)
	})
	return results, nil
}

func (lf *LocalFrameServer) ConnectionString() string {
	return LocalFrameServerUri(lf.Root)
}

func LocalFrameServerUri(root string) string {
	return "local_frames://" + root
}

type nullFrameServer struct {
}

func NewNullFrameServer() FrameServer {
	return &nullFrameServer{}
}

func (n nullFrameServer) FrameIntervals(frameType string, timeSpan TimeSpan) ([]TimeSpan, error) {
	return nil, errors.New("Not Implemented")
}

func (n nullFrameServer) FrameFilePaths(frameType string, timeSpan TimeSpan) ([]string, error) {
	return nil, errors.New("Not Implemented")
}

func (n nullFrameServer) ConnectionString() string {
	return "null_frames://null"
}

var frameServersLock sync.Mutex
var frameServers map[string]func(string) (FrameServer, error)

func init() {
	frameServers = make(map[string]func(string) (FrameServer, error))
	RegisterFrameServer("local_frames", func(path string) (FrameServer, error) {
		return NewLocalFrameServer(path), nil
	})
	RegisterFrameServer("null_frames", func(path string) (FrameServer, error) {
		return NewNullFrameServer(), nil
	})
}

func RegisterFrameServer(name string, generator func(string) (FrameServer, error)) {
	frameServersLock.Lock()
	defer frameServersLock.Unlock()

	frameServers[name] = generator
}

func GetFrameServer(name string) (FrameServer, error) {
	parts := strings.SplitN(name, "://", 2)

	frameServersLock.Lock()
	defer frameServersLock.Unlock()

	if generator, ok := frameServers[parts[0]]; ok {
		return generator(parts[1])
	}
	return nil, errors.New(fmt.Sprintf("Frame server %s could not be found", name))
}
