//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package server

import (
	"fmt"
	"git.ligo.org/nds/nds-metadata-server/common"
	"io"
	"os"
	"strconv"
	"strings"
)

type Epoch []byte

type epoch struct {
	name string
	span common.TimeSpan
}
type epochList []epoch

func defaultEpochs() epochList {
	return epochList{
		epoch{"S5", common.CreateTimeSpan(815153408, 880920032)},
		epoch{"S6", common.CreateTimeSpan(930960015, 971654415)},
		epoch{"S6a", common.CreateTimeSpan(930960015, 935798415)},
		epoch{"S6b", common.CreateTimeSpan(937785615, 947203215)},
		epoch{"S6c", common.CreateTimeSpan(947635215, 961545615)},
		epoch{"S6d", common.CreateTimeSpan(961545615, 971654415)},
		epoch{"ER2", common.CreateTimeSpan(1025636416, 1028563232)},
		epoch{"ER3", common.CreateTimeSpan(1042934416, 1045353616)},
		epoch{"ER4", common.CreateTimeSpan(1057881616, 1061856016)},
		epoch{"ER5", common.CreateTimeSpan(1073606416, 1078790416)},
		epoch{"ER6", common.CreateTimeSpan(1102089216, 1102863616)},
		epoch{"ER7", common.CreateTimeSpan(1116700672, 1116800000)},
		epoch{"ER8", common.CreateTimeSpan(1123856384, 1123900032)},
		epoch{"O1", common.CreateTimeSpan(1126621184, 1137254400)},
		epoch{"ER9", common.CreateTimeSpan(1151848448, 1152169216)},
		epoch{"ER10", common.CreateTimeSpan(1163173888, 1164554240)},
		epoch{"O2", common.CreateTimeSpan(1164554240, 1188081664)},
		epoch{"ER14", common.CreateTimeSpan(1235746816, 1238163456)},
		epoch{"O3", common.CreateTimeSpan(1238163456, common.GpsInf)},
	}
}

func loadEpochs(reader io.Reader) epochList {
	epochs := make(epochList, 0, 10)
	common.ProcessStreamWithComments(reader, func(input string) {
		parts := common.FilterOutEmpty(strings.Split(input, " "))
		if len(parts) != 3 {
			return
		}
		timeParts := strings.Split(parts[2], "-")
		if len(timeParts) != 2 {
			return
		}

		start, err1 := strconv.ParseInt(timeParts[0], 10, 64)
		stop, err2 := strconv.ParseInt(timeParts[1], 10, 64)
		if err1 != nil || err2 != nil {
			return
		}
		epochs = append(epochs, epoch{parts[0], common.CreateTimeSpan(start, stop)})
	})
	return epochs
}

func LoadEpochs(fname string) Epoch {
	var epochs epochList
	file, err := os.Open(fname)
	if err == nil {
		defer file.Close()
		epochs = loadEpochs(file)
	} else {
		epochs = defaultEpochs()
	}

	sep := ""
	var b strings.Builder
	for _, entry := range epochs {
		b.WriteString(sep)
		b.WriteString(fmt.Sprintf("%s=%d-%d", entry.name, entry.span.Start(), entry.span.End()))
		sep = " "
	}
	return []byte(b.String())
}
