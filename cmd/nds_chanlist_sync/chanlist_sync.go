package main

import (
	"flag"
	"fmt"
	"os"
)

type ServerConfig struct {
	ListenAddress string
	CacheDir      string
	SourceDir     string
}

type ClientConfig struct {
	SourceAddress string
	DestDir       string
	UpdateCmd     string
}

type Config struct {
	Server *ServerConfig
	Client *ClientConfig
}

func parseArgs() Config {
	serverFlags := flag.NewFlagSet("serve", flag.ExitOnError)

	serverListen := serverFlags.String("listen", "localhost:31207", "Address/port to listen on")
	serverCache := serverFlags.String("cache", ".sync_cache", "Cache directory to put gzipped files in")
	serverSource := serverFlags.String("source", ".", "Source directory to look for *ChanList.txt files in")

	clientFlags := flag.NewFlagSet("receive", flag.ExitOnError)
	clientSource := clientFlags.String("server", "localhost:31207", "host/port of the server")
	clientDest := clientFlags.String("dest", ".", "Destination directory")
	clientUpdate := clientFlags.String("on-update", "", "Command to call after updates are found")

	mainHelp := func() {
		fmt.Printf("Usage %s mode <options>\nWhere mode is\n\tserve - Serve chanlists\n\treceive - Receive chanlists\n", os.Args[0])
		fmt.Println("NOTE: this is a temporary utility that will go away.")
		os.Exit(1)
	}

	if len(os.Args) < 2 || (os.Args[1] != serverFlags.Name() && os.Args[1] != clientFlags.Name()) {
		mainHelp()
	}
	var server *ServerConfig
	var client *ClientConfig
	if os.Args[1] == serverFlags.Name() {
		_ = serverFlags.Parse(os.Args[2:])
		server = &ServerConfig{
			ListenAddress: *serverListen,
			CacheDir:      *serverCache,
			SourceDir:     *serverSource,
		}
	} else {
		_ = clientFlags.Parse(os.Args[2:])
		client = &ClientConfig{
			SourceAddress: *clientSource,
			DestDir:       *clientDest,
			UpdateCmd:     *clientUpdate,
		}
	}

	if (client == nil && server == nil) || (client != nil && server != nil) {
		panic("Invalid internal state found")
	}

	return Config{
		Server: server,
		Client: client,
	}
}

func main() {
	mainCfg := parseArgs()
	if mainCfg.Server != nil {
		runServer(mainCfg.Server)
	} else {
		runClient(mainCfg.Client)
	}
}
