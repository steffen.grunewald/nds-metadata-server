package main

import (
	"compress/gzip"
	"crypto/sha256"
	"encoding/hex"
	"io"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"time"
)

func waitTime() time.Duration {
	return time.Minute * 10
}

func requireDirectory(path string) {
	info, err := os.Stat(path)
	if err != nil {
		log.Fatalf("Error while checking dir, %v", err)
	}
	if !info.IsDir() {
		log.Fatalf("the %s must be a directory", path)
	}
}

func reFilePredicate(r *regexp.Regexp) func(os.FileInfo) bool {
	return func(entry os.FileInfo) bool {
		return r.MatchString(entry.Name())
	}
}

func getFilenames(directory string, pred func(os.FileInfo) bool) ([]string, error) {
	results := make([]string, 0, 0)
	files, err := ioutil.ReadDir(directory)
	if err != nil {
		return nil, err
	}
	for _, entry := range files {
		if entry.IsDir() {
			continue
		}
		if pred(entry) {
			results = append(results, entry.Name())
		}
	}
	return results, nil
}

func hashToString(hashVal []byte) string {
	return hex.EncodeToString(hashVal)
}

func copyAndChecksum(input io.Reader, dest string, compress bool) (string, error) {
	h := sha256.New224()

	tmpName := dest + ".tmp"
	defer os.Remove(tmpName)
	err := func() error {
		tmp, err := os.Create(tmpName)
		if err != nil {
			return err
		}
		defer tmp.Close()
		var out io.Writer
		out = io.MultiWriter(h, tmp)
		if compress {
			gzOut, err := gzip.NewWriterLevel(io.MultiWriter(h, tmp), gzip.BestCompression)
			if err != nil {
				return err
			}
			defer gzOut.Close()
			out = gzOut
		}
		if _, err := io.Copy(out, input); err != nil {
			return err
		}
		return nil
	}()
	if err != nil {
		return "", err
	}
	if err = os.Rename(tmpName, dest); err != nil {
		return "", err
	}
	return hashToString(h.Sum(nil)), nil
}
