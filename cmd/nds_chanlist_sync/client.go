package main

import (
	"crypto/sha256"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"regexp"
	"time"
)

type ClientFile struct {
	FrameType string
	Path      string
	Checksum  string
}

func clientChecksumFile(path string) string {
	f, err := os.Open(path)
	if err != nil {
		return ""
	}
	defer f.Close()
	h := sha256.New224()
	_, err = io.Copy(h, f)
	if err != nil {
		return ""
	}
	return hashToString(h.Sum(nil))
}

func clientProcessFile(cfg *ClientConfig, file string) ClientFile {
	fullPath := path.Join(cfg.DestDir, file)
	return ClientFile{
		FrameType: clientGetFrameType(file),
		Path:      fullPath,
		Checksum:  clientChecksumFile(fullPath),
	}
}

var clientTextRe *regexp.Regexp

func init() {
	clientTextRe = regexp.MustCompile("^(?P<frameType>[A-Z0-9a-z\\-_]+)-ChanList.txt.gz$")
}

func clientGetFrameType(filename string) string {
	return clientTextRe.FindStringSubmatch(filename)[1]
}

func clientUpdate(updaterAddress string, oldEntry ClientFile) (ClientFile, bool) {
	url := fmt.Sprintf("http://%s/%s?checksum=%s", updaterAddress, oldEntry.FrameType, oldEntry.Checksum)
	resp, err := http.Get(url)
	if err != nil {
		return oldEntry, false
	}
	defer resp.Body.Close()
	// if our checksum is ok, we get a 304
	if resp.StatusCode != http.StatusOK {
		return oldEntry, false
	}
	checksum, err := copyAndChecksum(resp.Body, oldEntry.Path, false)
	if err != nil {
		return oldEntry, false
	}
	// only do work on a 200 OK
	return ClientFile{
		FrameType: oldEntry.FrameType,
		Path:      oldEntry.Path,
		Checksum:  checksum,
	}, true
}

func runCommand(cmd string) error {
	c := exec.Command(cmd)
	return c.Run()
}

func runClient(cfg *ClientConfig) {
	requireDirectory(cfg.DestDir)

	log.Printf("Scanning %s for *ChanList.txt.gz files", cfg.DestDir)
	files, err := getFilenames(cfg.DestDir, reFilePredicate(clientTextRe))
	if err != nil {
		log.Fatalf("Error while scanning files, %v", err)
	}
	if len(files) == 0 {
		log.Fatalf("No files found, aborting, nothing to do")
	}

	clientState := make([]ClientFile, len(files))
	for i, entry := range files {
		clientState[i] = clientProcessFile(cfg, entry)
		log.Printf("Tracking '%s' current hash %s", clientState[i].FrameType, clientState[i].Checksum)
	}

	for {
		didUpdates := false
		for i, entry := range clientState {
			updatedEntry, ok := clientUpdate(cfg.SourceAddress, entry)
			if ok {
				log.Printf("Updated %s, %s", updatedEntry.FrameType, updatedEntry.Checksum)
				clientState[i] = updatedEntry
				didUpdates = true
			}
		}

		if didUpdates && cfg.UpdateCmd != "" {
			if err := runCommand(cfg.UpdateCmd); err != nil {
				log.Printf("Command '%s' failed, %v", cfg.UpdateCmd, err)
			} else {
				log.Printf("Update command '%s' run successfully", cfg.UpdateCmd)
			}
		}
		time.Sleep(waitTime())
	}
}
