//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package nds2parser

import (
	"errors"
	"git.ligo.org/nds/nds-metadata-server/common"
	"strings"
)

type SourceSpan struct {
	FrameType string
	Start     common.GpsSecond
	End       common.GpsSecond
}

type NdsChannelListEntry struct {
	common.BasicChannel
	Spans []SourceSpan
}

type channelListState struct {
	Data      string
	Name      string
	ClassType common.ClassType
	DataType  common.DataType
	PosInt    int64
	Float     float64
	FrameType string
	Channel   common.BasicChannel
	Spans     []SourceSpan
}

func CreateChannelListState(input string) *channelListState {
	return &channelListState{Data: input,
		ClassType: common.CreateClassType(common.ClassTypeUnknown),
		DataType:  common.CreateDataType(common.DataTypeUnknown),
		Spans:     make([]SourceSpan, 0, 0)}
}

func (s *channelListState) Save() string {
	return s.Data
}

func (s *channelListState) Restore(input string) {
	s.Data = input
}

func (s *channelListState) RestoreIfFailed(input string, pred *bool) {
	if !*pred {
		s.Data = input
	}
}

func (s *channelListState) Str() string {
	return s.Data
}

func (s *channelListState) SetStr(input string) {
	s.Data = input
}

func (s *channelListState) ChannelNameParsed(name string) {
	s.Name = name
}

func (s *channelListState) ClassTypeParsed(val byte) {
	s.ClassType.Set(val)
}

func (s *channelListState) DataTypeParsed(val byte) {
	s.DataType.Set(val)
}

func (s *channelListState) PosIntParsed(val int64) {
	s.PosInt = val
}

func (s *channelListState) FloatParsed(val float64) {
	s.Float = val
}

func (s *channelListState) FrameTypeParsed(frameType string) {
	s.FrameType = frameType
}

func (s *channelListState) SourceSpanParsed(span SourceSpan) {
	s.Spans = append(s.Spans, span)
}

func (s *channelListState) ChannelParsed(channel common.BasicChannel) {
	s.Channel = channel
}

func (cl *NdsChannelListEntry) Existence() common.TimeSpan {
	var extents common.TimeSpan
	for _, entry := range cl.Spans {
		extents = common.CombineTimeSpans(extents, common.CreateTimeSpan(entry.Start, entry.End))
	}
	return extents
}

func ChannelListLine(input string) (NdsChannelListEntry, error) {
	state := CreateChannelListState(input)
	if parseChannelListLine(state) {
		return NdsChannelListEntry{state.Channel, state.Spans}, nil
	}
	return NdsChannelListEntry{}, errors.New("Unable to parse")
}

func parseChannelListLine(state *channelListState) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	result = false

	if !parseChannelName(state) {
		return
	}
	name := state.Name
	if !parseSpace(state) {
		return
	}
	if !parseClassType(state) {
		return
	}
	classType := state.ClassType
	if !parseSpace(state) {
		return
	}
	if !parsePosInt(state) {
		return
	}
	if !parseSpace(state) {
		return
	}
	if !parseDataType(state) {
		return
	}
	dataType := state.DataType
	if !parseSpace(state) {
		return
	}
	if !parseFloat(state) {
		return
	}
	rate := state.Float
	if !parseSourceSpanList(state) {
		return
	}
	state.Data = trimBlanks(state.Data)
	if len(state.Data) != 0 {
		return
	}
	state.ChannelParsed(common.BasicChannel{
		Name:  name,
		Type:  dataType,
		Class: classType,
		Rate:  rate,
	})
	result = true
	return
}

func parseSourceSpanList(state *channelListState) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	result = false
	if !parseSourceSpan(state) {
		return
	}
	for {
		if !parseSourceSpan(state) {
			break
		}
	}
	result = true
	return
}

func parseSourceSpan(state *channelListState) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	result = false

	if !parseSpace(state) {
		return
	}
	if !parseFrameType(state) {
		return
	}
	frameType := state.FrameType
	start := int64(0)
	end := int64(0)

	parseSpan := func() bool {
		if !parseSpace(state) {
			return false
		}
		if !parsePosInt(state) {
			return false
		}
		start = state.PosInt
		var ok bool
		if state.Data, ok = consumeIfMatches(state.Data, ":"); !ok {
			return false
		}
		if !parsePosInt(state) {
			return false
		}
		end = start + state.PosInt
		return true
	}
	if !parseSpan() {
		start = 0
		end = 0
	}
	state.SourceSpanParsed(SourceSpan{FrameType: frameType, Start: start, End: end})
	result = true
	return
}

func parseFrameType(state *channelListState) bool {
	const allowed = alphaNum + "-_"

	trimmed := strings.TrimLeft(state.Data, alpha)
	if len(trimmed) == len(state.Data) {
		return false
	}
	trimmed = strings.TrimLeft(trimmed, allowed)
	delta := len(state.Data) - len(trimmed)
	state.FrameTypeParsed(state.Data[:delta])
	state.Data = trimmed
	return true
}
