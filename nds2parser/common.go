//
// Tools for accessing and serving out gravitational wave data over the nds protocols
//
// Copyright 2020 California Institute of Technology.
//
// This program is free software and is distributed in the hope that it will be
// useful; you may redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation
// (http://www.gnu.org/licenses); version 2 (GPLv2) of the License or at
// your discretion, any later version (i.e GPLv3).
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
// ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Neither the names of the California Institute of Technology (Caltech), The
// Massachusetts Institute of Technology (M.I.T), The Laser Interferometer
// Gravitational-Wave Observatory (LIGO), nor the names of its contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
//
// You should have received a copy of the licensing terms for this software
// included in the file “LICENSE” located in the top-leveldirectory of this
// package.If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE.txt
//

package nds2parser

import (
	"strconv"
	"strings"
)

import (
	"git.ligo.org/nds/nds-metadata-server/common"
)

const (
	blanks            = " \t"
	semicolonLiteral  = ";"
	openBraceLiteral  = "{"
	closeBraceLiteral = "}"
	digits            = "0123456789"
	floatBits         = digits + "."
	alpha             = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	alphaNum          = alpha + digits
	chanNameVals      = alphaNum + ":-_./~"
)

type Command struct {
	Command int
}

func CreateCommand(cmd int) Command {
	var c Command
	c.Set(cmd)
	return c
}

func (c *Command) Set(cmd int) {
	if cmd >= 0 && cmd <= CommandUnknown {
		c.Command = cmd
	} else {
		c.Command = CommandUnknown
	}
}

func (c *Command) Get() int {
	return c.Command
}

type ParsedCommand struct {
	Command   Command
	Version   int
	Revision  int
	ClassType common.ClassType
	GpsTime   common.GpsSecond
	TimeSpan  common.TimeSpan
	Pattern   BashPattern
	Channels  []common.BasicChannel
}

type parserState interface {
	Save() string
	Restore(input string)
	RestoreIfFailed(input string, pred *bool)

	Str() string
	SetStr(input string)
}

type posIntParser interface {
	parserState
	PosIntParsed(time common.GpsSecond)
}

type channelNameParser interface {
	parserState
	ChannelNameParsed(name string)
}

type floatParser interface {
	parserState
	FloatParsed(val float64)
}

type dataTypeParser interface {
	parserState
	DataTypeParsed(dataType byte)
}

type classTypeParser interface {
	parserState
	ClassTypeParsed(classType byte)
}

type patternParser interface {
	parserState
	PatternParsed(pattern BashPattern)
}

type classMapping struct {
	Name      string
	ClassType byte
}
type dataMapping struct {
	Name     string
	DataType byte
}

var classMappings []classMapping
var dataMappings []dataMapping

func init() {
	classMappings = []classMapping{
		{"online", common.ClassTypeOnline},
		{"raw", common.ClassTypeRaw},
		{"reduced", common.ClassTypeReduced},
		{"s-trend", common.ClassTypeSTrend},
		{"m-trend", common.ClassTypeMTrend},
		{"test-pt", common.ClassTypeTestPt},
		{"static", common.ClassTypeStatic},
		{"unknown", common.ClassTypeUnknown},
	}
	dataMappings = []dataMapping{
		{"int_2", common.DataTypeInt16},
		{"int_4", common.DataTypeInt32},
		{"int_8", common.DataTypeInt64},
		{"real_4", common.DataTypeFloat32},
		{"real_8", common.DataTypeFloat64},
		{"complex_8", common.DataTypeComplex32},
		{"uint_4", common.DataTypeUint32},
		{"unknown", common.DataTypeUnknown},
	}
}

func trimBlanks(input string) string {
	return strings.TrimLeft(input, blanks)
}

func consume(input string, count int) string {
	return input[count:]
}

func consumeIfMatches(input string, prefix string) (string, bool) {
	if strings.HasPrefix(input, prefix) {
		return consume(input, len(prefix)), true
	} else {
		return input, false
	}
}

func parseSpace(state parserState) bool {
	trimmed := trimBlanks(state.Str())
	if len(trimmed) == len(state.Str()) {
		return false
	}
	state.SetStr(trimmed)
	return true
}

func parsePosInt(state posIntParser) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	result = false

	trimmed := strings.TrimLeft(state.Str(), digits)
	delta := len(state.Str()) - len(trimmed)
	if delta == 0 {
		return
	}
	val, err := strconv.ParseInt(state.Str()[:delta], 10, 64)
	if err != nil {
		return
	}
	state.PosIntParsed(val)
	state.SetStr(trimmed)
	result = true
	return
}

func parseClassType(state classTypeParser) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	result = false

	for _, mapping := range classMappings {
		if trimmed, ok := consumeIfMatches(state.Str(), mapping.Name); ok {
			result = true
			state.SetStr(trimmed)
			state.ClassTypeParsed(mapping.ClassType)
			return
		}
	}
	return
}

func parseDataType(state dataTypeParser) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	result = false

	for _, mapping := range dataMappings {
		if trimmed, ok := consumeIfMatches(state.Str(), mapping.Name); ok {
			result = true
			state.SetStr(trimmed)
			state.DataTypeParsed(mapping.DataType)
			return
		}
	}
	return
}

func parseFloat(state floatParser) (result bool) {
	result = false

	data := state.Str()
	trimmed := strings.TrimLeft(data, floatBits)
	delta := len(data) - len(trimmed)
	if delta == 0 {
		return
	}
	val, err := strconv.ParseFloat(data[:delta], 64)
	if err != nil {
		return
	}
	state.FloatParsed(val)
	state.SetStr(trimmed)
	result = true
	return
}

func parseChannelName(state channelNameParser) (result bool) {
	defer state.RestoreIfFailed(state.Save(), &result)
	result = false

	initial := state.Str()
	trimmed := strings.TrimLeft(initial, alpha)
	if len(trimmed) == len(initial) {
		return
	}
	trimmed = strings.TrimLeft(trimmed, chanNameVals)
	state.ChannelNameParsed(initial[:len(initial)-len(trimmed)])
	state.SetStr(trimmed)

	result = true
	return
}

func parseOpenBrace(state parserState) bool {
	if data, ok := consumeIfMatches(state.Str(), openBraceLiteral); ok {
		state.SetStr(data)
		return true
	}
	return false
}

func parseCloseBrace(state parserState) bool {
	if data, ok := consumeIfMatches(state.Str(), closeBraceLiteral); ok {
		state.SetStr(data)
		return true
	}
	return false
}
